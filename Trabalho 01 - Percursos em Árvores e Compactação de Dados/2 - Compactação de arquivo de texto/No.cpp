/* 
 * File:   No.cpp
 * 
 * Created on 21 de Abril de 2018, 21:43
 */

#include <stddef.h>

#include "No.h"

No::No() {
    esq = NULL;
    dir = NULL;
}

No::No(No* no1, No* no2) {
    esq = no1;
    dir = no2;
    ch = '+';
    freq = (esq->GetFreq() + dir->GetFreq());
}

No::No(char ch) {
    this->ch = ch;
    freq = 0;
    esq = NULL;
    dir = NULL;
}

No::No(char ch, int freq) {
    this->ch = ch;
    this->freq = freq;
    esq = NULL;
    dir = NULL;
}

No::No(const No& orig) {
}

No::~No() {
    delete esq;
    delete dir;
}

void No::SetFreq(int freq) {
    this->freq = freq;
}

int No::GetFreq() const {
    return freq;
}

void No::SetCh(char ch) {
    this->ch = ch;
}

char No::GetCh() const {
    return ch;
}

void No::SetDir(No* dir) {
    this->dir = dir;
}

No* No::GetDir() const {
    return dir;
}

void No::SetEsq(No* esq) {
    this->esq = esq;
}

No* No::GetEsq() const {
    return esq;
}

