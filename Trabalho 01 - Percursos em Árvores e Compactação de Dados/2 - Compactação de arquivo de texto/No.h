/* 
 * File:   No.h
 *
 * Created on 21 de Abril de 2018, 21:43
 */

#ifndef NO_H
#define NO_H

class No {
public:
    No();
    No(No* no1, No* no2);
    No(char ch);
    No(char ch, int freq);
    No(const No& orig);
    virtual ~No();
    void SetFreq(int freq);
    int GetFreq() const;
    void SetCh(char ch);
    char GetCh() const;
    void SetDir(No* dir);
    No* GetDir() const;
    void SetEsq(No* esq);
    No* GetEsq() const;
private:
    No* esq;
    No* dir;
    char ch;
    int freq;
};

#endif /* NO_H */

