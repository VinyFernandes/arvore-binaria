/* 
 * File:   main.cpp
 *
 * Created on 21 de Abril de 2018, 21:43
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <locale>

#include "Huffman.h"
#include "Graphics.h"

using namespace std;

//Escopos dos métodos internos
string LerEntrada(char argv[]);
string EscreverSaida(char nome_arquivo[], string entrada, map<char, string> tabela_codigo_caracter);

int main(int argc, char* argv[]) {
    setlocale(LC_ALL, "Portuguese");

    Huffman huff;
    string erro;

    if (argv[1] == NULL) {
        erro = "Ops... Informe o nome do arquivo de ENTRADA à frente do comando de execução. Por exemplo: ./codifica__o_de_huffman input.txt";
        ContentInstrucoes();
        ContentErro(erro);
        exit(-1);
    }
    if (argv[2] == NULL) {
        erro = "Ops... Informe o nome do arquivo de SAÍDA após o arquivo de ENTRADA. Por exemplo: ./codifica__o_de_huffman input.txt output.txt";
        ContentInstrucoes();
        ContentErro(erro);
        exit(-1);
    }

    string entrada = LerEntrada(argv[1]); //1º PASSO: Ler arquivo de entrada.
    list<No*> lista = huff.ContFrequencias(entrada); // 2º PASSO: Criar lista de Nós com Caracter|Frequencia
    huff.SetRaiz(huff.CriarArvore(lista)); // 3º PASSO: Criar Árvore de Huffman
    map<char, string> tabela_codigo_caracter = huff.Percurso(huff.GetRaiz(), ""); //4º PASSO: Criar tabela de Codigo|Caracter
    string saida = EscreverSaida(argv[2], entrada, tabela_codigo_caracter); //5º PASSO: Escrever o texto codificado no arquivo de saída

    BarraComprimindo();
    ContentSucesso(entrada, saida, tabela_codigo_caracter);

    return 0;
}

string LerEntrada(char argv[]) {
    ifstream arquivo(argv, ios::in);
    string linha, s = "";

    if (!arquivo.is_open()) {
        string erro = "Ops... Arquivo de ENTRADA não encontrado. Verifique o nome ou o caminho onde se encontra o arquivo, "
                "e tente novamente.";
        ContentInstrucoes();
        ContentErro(erro);
        exit(-1);
    }
    while (!arquivo.eof()) {
        getline(arquivo, linha);
        s += linha;
    }
    if (s.size() <= 1) {
        string erro = "Ops.. O arquivo de Entrada está vazio ou é muito pequeno "
                "para ser comprimido.";
        ContentInstrucoes();
        ContentErro(erro);
        exit(-1);
    }
    arquivo.close();
    return s;
}

string EscreverSaida(char nome_arquivo[], string entrada, map<char, string> tabela_codigo_caracter) {
    ofstream arquivo(nome_arquivo, ios::in);
    string saida = "";

    if (!arquivo.is_open()) {
        string erro = "Ops... Arquivo de SAÍDA não encontrado. Verifique o nome ou o caminho onde se encontra o arquivo, "
                "e tente novamente.";
        ContentInstrucoes();
        ContentErro(erro);
        exit(-1);
    }

    map<char, string>::iterator it = tabela_codigo_caracter.begin();
    for (int i = 0; i < entrada.size(); i++) {
        char ch = entrada[i];

        it = tabela_codigo_caracter.find(ch);
        if (it != tabela_codigo_caracter.end())
            saida.append(it->second);
    }
    arquivo << saida;
    arquivo.close();
    return saida;
}
