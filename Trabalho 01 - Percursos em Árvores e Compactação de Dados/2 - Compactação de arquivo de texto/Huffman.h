/* 
 * File:   Huffman.h
 *
 * Created on 22 de Abril de 2018, 22:14
 */

#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <iostream>
#include <map>
#include <iterator>
#include <string>
#include <list>

#include "No.h"

using namespace std;

class Huffman {
public:
    Huffman();
    Huffman(const Huffman& orig);
    virtual ~Huffman();
    list<No*> ContFrequencias(string texto);
    No* CriarArvore(list<No*> lista);
    map<char, string> Percurso(No* raiz, string codigo);
    string Codificar(string texto);
    void SetRaiz(No* raiz);
    No* GetRaiz() const;
private:
    No* raiz;
};

#endif /* HUFFMAN_H */

