/* 
 * File:   No.cpp
 * 
 * Created on 23 de Abril de 2018, 15:14
 */

#include <stddef.h>

#include "No.h"

No::No() {
    esq = NULL;
    dir = NULL;
}

No::No(int chave) {
    this->chave = chave;
    esq = NULL;
    dir = NULL;
}

No::No(const No& orig) {
}

No::~No() {
    delete esq;
    delete dir;
}

void No::SetChave(int chave) {
    this->chave = chave;
}

int No::GetChave() const {
    return chave;
}

void No::SetDir(No* dir) {
    this->dir = dir;
}

No* No::GetDir() const {
    return dir;
}

void No::SetEsq(No* esq) {
    this->esq = esq;
}

No* No::GetEsq() const {
    return esq;
}

