/* 
 * File:   No.h
 *
 * Created on 23 de Abril de 2018, 15:14
 */

#ifndef NO_H
#define NO_H

class No {
public:
    No();
    No(int chave);
    No(const No& orig);
    virtual ~No();
    void SetChave(int chave);
    int GetChave() const;
    void SetDir(No* dir);
    No* GetDir() const;
    void SetEsq(No* esq);
    No* GetEsq() const;
private:
    No* esq;
    No* dir;
    int chave;
};

#endif /* NO_H */

